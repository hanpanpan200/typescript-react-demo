# Front End Scaffold

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

This project was bootstrapped with [Create React App](https://create-react-app.dev/), you can get more details from [Create React App README](./doc/create-react-app-README.md)

You can find a demo in `./src/pages/` folder.

## Tech Stack

- User interface Building: [React](https://github.com/redux-saga/redux-saga)
- State management: [redux](https://redux.js.org) and [reselect](https://github.com/reduxjs/reselect)
- Side effect management: [redux-saga](https://github.com/redux-saga/redux-saga)
- Language: [Typescript](https://www.typescriptlang.org)
- Style management: [sass](https://sass-lang.com/documentation/syntax) and  [css-modules](https://github.com/css-modules/css-modules)
- For I18n: [react-i18next](https://react.i18next.com)
- Router: [react router](https://reacttraining.com/react-router/web/guides/quick-start)


## Quick Guide

### Dependencies

- [yarn](https://yarnpkg.com/lang/en/) (1.19.1)
- [node](https://nodejs.org/en/) (12.13.0)

### Commands

- Install the project: `yarn`
- Run the dev server: `yarn start`
- Run the test: `yarn test`
- Run a production build: `yarn build`

## Conventions

### Commit message guideline

We have git commit message guideline which draws inspiration from [Angular Commit Message Guidelines](https://gist.github.com/stephenparish/9941e89d80e2bc58a153#format-of-the-commit-message).

#### Goals

- Allow generating CHANGELOG.md by script
- Provide better information when browse the history
- Unified commit message format in a flexible way
- Link JIRA ID on commit message to see details for issue with JIRA integration on Bitbucket

#### Format

`<type>(<jira_id>): <scope>`

The `type` must be one of the following:

- build: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- ci: Changes to our CI configuration files and scripts (example scopes: Circle, BrowserStack, SauceLabs)
- docs: Documentation only changes
- feat: A new feature
- fix: A bug fix
- perf: A code change that improves performance
- refactor: A code change that neither fixes a bug nor adds a feature
- style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- test: Adding missing tests or correcting existing tests
- release: Changes about release such as bumping the version in package.json

Note:

If your card is not related to any JIRA card, just use `#N/A` for the `<jira_id>`.

#### Example

```
feat(SP-1): add button component
fix(SP-2): adjust animation effects
fix(#N/A): UI enhancement for button
release(#N/A): new version 1.0.0
```

### Folder structure

```
  '    |-- app.tsx',
  '    |-- configureStore.ts',
  '    |-- index.scss',
  '    |-- index.tsx',
  '    |-- reducer.ts',
  '    |-- react-app-env.d.ts',
  '    |-- assets':
  '    |   |-- styles',
  '    |       |-- reset.scss',
  '    |       |-- mixins',
  '    |           |-- animation.scss',
  '    |           |-- text-truncate.scss',
  '    |-- components',
  '    |   |-- TextButton',
  '    |       |-- index.module.scss',
  '    |       |-- index.tsx',
  '    |-- i18n',
  '    |   |-- en.ts',
  '    |   |-- index.ts',
  '    |-- page',
  '    |   |-- index.tsx',
  '    |   |-- reducer.ts',
  '    |   |-- todo',
  '    |       |-- actions.ts',
  '    |       |-- index.tsx',
  '    |       |-- reducer.ts',
  '    |       |-- selector.ts',
  '    |       |-- types.ts',
  '    |       |-- add',
  '    |       |   |-- index.module.scss',
  '    |       |   |-- index.tsx',
  '    |       |-- list',
  '    |           |-- index.module.scss',
  '    |           |-- index.tsx',
  '    |           |-- Item',
  '    |               |-- index.tsx',
  '    |-- utils',
  '        |-- httpService.ts',
```

Special Notes:

- Folder `src/components` is only used for **shared common components**, we'll migrate those components to a UI library in future when our components are stable.  

- Feature components should be added in feature module.


### Naming convention

- Files and Folders:
    - File and folder names should be written in camel-case .
    - File and folder names should start with lower case letters with one exception: files and folders containing components.
- Variables:
    - Variable names should be written in camel-case.
    - Variable names should start with lower case letters with two exceptions: component and class names.
    - Do not prefix interface with 'I'.
    - Start a selector name with 'select', for example: selectTodos.
- Constants:
    - Constant names should be written in snake-case with all upper case letters like: DEV_PORT.

## Recommendations

- Use **absolute package resolution** when the target file is not the direct child or parent when importing both the ts and sass packages. Check this [commit](https://bitbucket.org/hanpanpan200/typescript-react-demo/commits/ac4d681d4fba8c799d974f4e25f3e6b5ab867776) for the detail.     
- Use functional component with [React Hooks](https://reactjs.org/docs/hooks-intro.html).
- All devs work on `master` branch. For more information, please refer to [Trunk Based Development](https://trunkbaseddevelopment.com/)

## Tools

- Chrome plugin: [Redux DevTools extension](https://github.com/zalmoxisus/redux-devtools-extension)
- Chrome plugin: [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)

