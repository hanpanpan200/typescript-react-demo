import React, { lazy, Suspense } from 'react';
import { Provider } from 'react-redux';
import store from './configureStore';
import {
  BrowserRouter as Router,
} from 'react-router-dom';

const Page = lazy(() => import('./pages'));

const App = () => (
  <Provider store={store}>
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Page/>
      </Suspense>
    </Router>
  </Provider>
);

export default App;
