// An example for:
//  1. How to use mixin style

import React from 'react';
import styles from './index.module.scss';

interface TextBtnProps {
  btnTxt: string;
  titleTxt?: string;
}

interface TextBtnState {
  withAnim: string;
}

const ANIMATION_TYPES = [
  'fadeIn',
  'bounceIn',
  'blurIn',
  'flipxIn',
  'rubber',
  'newspaper',
  'tada',
  'foolishOut',
];

export default class TextButton extends React.Component<
  TextBtnProps,
  TextBtnState
> {
  static defaultProps = {
    titleTxt: 'Animation examples',
  };

  constructor(props: TextBtnProps) {
    super(props);

    this.state = {
      withAnim: '',
    };
  }

  onHandleBtnClick = (type: string) => {
    this.setState({
      withAnim: type,
    });
  }

  render() {
    const { titleTxt } = this.props;
    const { withAnim } = this.state;
    return (
      <div className={styles.txtContainer}>
        {titleTxt ? <p className={styles.tcTxt}>{titleTxt}</p> : null}
        {ANIMATION_TYPES.map((type) => {
          return (
            <button
              onClick={() => {
                this.onHandleBtnClick(type);
              }}
              className={styles.tcBtn}
              key={type}
            >
              {type}
            </button>
          );
        })}
        <img
          alt='logo'
          src='https://thoughtworks.okta.com/fs/bco/1/fs01awsi9a9eoGHI20h8'
          className={styles[withAnim]}
        />
      </div>
    );
  }
}
