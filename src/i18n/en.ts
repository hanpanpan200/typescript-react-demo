export default {
  translation: {
    confirm: 'Confirm',
    todo: {
      addTodoTitle: 'Add a Todo manually',
      listPageTitle: 'Todo List',
      requestRemoteTodoTitle: 'Request remote Todo',
    },
  },
};
