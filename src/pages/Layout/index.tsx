import React from 'react';
import styles from './index.module.scss';

interface Props {
  children: React.ReactNode;
}

const Layout: React.FC<Props> = ({children}) => (
  <div>
    <header
      className={styles.header}
    >
      this is the header
    </header>
      {children}
    <footer
      className={styles.footer}
    >
      this is the footer
    </footer>
  </div>
);

export default Layout;
