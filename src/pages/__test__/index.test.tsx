import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { configure, shallow } from 'enzyme';
import Index from '../';

configure({adapter: new Adapter()});

describe('Pages <Index />', () => {
  it('should render corractly', () => {
    const wrapper = shallow(<Index />);
    expect(wrapper).toMatchSnapshot();
  });
});
