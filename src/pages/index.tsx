import React from 'react';
import { Route as LibRoute, Switch } from 'react-router-dom';
import Todo from './todo';
import Layout from './Layout';

const Route = () => {
  return (
    <Layout>
      <Switch>
        <LibRoute path='/todo' component={Todo} />
      </Switch>
    </Layout>
  );
};

export default Route;
