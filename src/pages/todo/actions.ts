import {
  ADD_TODO,
  REQUEST_TODO_LIST,
  REQUEST_TODO_LIST_COMPLETED,
  AddTodoActionType,
  RequestTodoListActionType,
  RequestTodoListCompleteType,
} from './types';

export const addTodoAction = (text: string): AddTodoActionType => ({
  type: ADD_TODO,
  payload: {
    text,
    id: Date.now(),
  },
});

export const requestTodoListAction = (): RequestTodoListActionType => ({
  type: REQUEST_TODO_LIST,
  payload: null,
});

export const requestCompletedAction = (remoteTodoList: []): RequestTodoListCompleteType => ({
  type: REQUEST_TODO_LIST_COMPLETED,
  payload: remoteTodoList,
});
