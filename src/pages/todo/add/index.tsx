import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Trans as T } from 'react-i18next';
import { addTodoAction } from '../actions';
import styles from './index.module.scss';

interface Props {
  addTodo: typeof addTodoAction;
}

const Component: React.FC<Props> = ({ addTodo }) => {
  const [text, setText] = useState<string>('');
  return (
    <div className={styles.addTodoPage}>
      <header>
        <h1>
          <T>todo.addTodoTitle</T>
        </h1>
      </header>
      <section>
        <input
          value={text}
          onChange={({ target: { value } }) => setText(value)}
        />
        <button
          onClick={() => {
            addTodo(text);
            setText('');
          }}
        >
          <T>confirm</T>
        </button>
      </section>
      <footer>
        <Link to='/todo/list'>
          <T>todo.listPageTitle</T>
        </Link>
      </footer>
    </div>
  );
};

const mapDispatchToProps = {
  addTodo: addTodoAction,
};

export default connect(
  null,
  mapDispatchToProps,
)(Component);
