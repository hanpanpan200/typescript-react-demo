import React from 'react';
import { Trans as T } from 'react-i18next';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AppState } from '../../../reducer';
import { selectTodos } from '../selector';
import { Todo } from '../types';
import Item from './Item';
import { requestTodoListAction } from '../actions';
import styles from './index.module.scss';

interface Props {
  todos: Todo[];
  requestTodoList: typeof requestTodoListAction;
}

const Component: React.FC<Props> = ({ todos, requestTodoList }) => {
  const handleRequest = () => {
    requestTodoList();
  };

  return (
    <div className={styles.todoListPage}>
      <header>
        <h1>
          <T>todo.listPageTitle</T>
        </h1>
      </header>
      <button onClick={handleRequest}><T>todo.requestRemoteTodoTitle</T></button>
      <ul>
        {todos.map(({ id, ...rest }, index) => (
          <Item key={`${id}_${index}`} id={id} {...rest} />
        ))}
      </ul>
      <footer>
        <Link to='/todo/add'>
          <T>todo.addTodoTitle</T>
        </Link>
      </footer>
    </div>
  );
};

const mapDispatchToProps = (dispatch: Dispatch) => ({
  requestTodoList: () => dispatch(requestTodoListAction()),
});

const mapStateToProps = (state: AppState) => ({
  todos: selectTodos(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
