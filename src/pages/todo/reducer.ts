import { todoState, AddTodoActionType, RequestTodoListCompleteType, ADD_TODO, REQUEST_TODO_LIST_COMPLETED } from './types';

const initState: todoState = [];

function reducer(state = initState, action: AddTodoActionType | RequestTodoListCompleteType): todoState {
  switch (action.type) {
    case ADD_TODO:
      return [...state, action.payload];
    case REQUEST_TODO_LIST_COMPLETED:
      return [...state, ...action.payload];
    default:
      return state;
  }
}

export default reducer;
