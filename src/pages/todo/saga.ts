import { call, takeEvery, put } from 'redux-saga/effects';
import { REQUEST_TODO_LIST  } from './types';
import { requestCompletedAction } from './actions';
import Api from 'utils/api';

export function* requestTodoList() {
  const remoteTodoList = yield call(Api.queryTodoList);
  yield put(requestCompletedAction(remoteTodoList));
}

export function* watchRequestTodoList() {
  yield takeEvery(
    REQUEST_TODO_LIST,
    requestTodoList,
  );
}
