import { AppState } from 'reducer';
import { Todo } from './types';

export const selectTodos = (state: AppState): Todo[] => state.page.todo;
