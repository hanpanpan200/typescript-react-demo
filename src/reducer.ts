import { combineReducers } from 'redux';
import { reducer as pageReducer } from './pages/reducer';

const rootReducer = combineReducers({
  page: pageReducer,
});

export default rootReducer;

export type AppState = ReturnType<typeof rootReducer>;
