import { todoState } from '../pages/todo/types';

export const queryTodoList = (): Promise<todoState> => {
  const promise = new Promise<todoState>((resolve) => {
    setTimeout(() => {
      resolve([
        {
          id: new Date().getTime(),
          text: 'todo 1',
        },
        {
          id: new Date().getTime(),
          text: 'todo 2',
        },
      ]);
    }, 2000);
  });
  return promise;
};

export default {
  queryTodoList,
};
