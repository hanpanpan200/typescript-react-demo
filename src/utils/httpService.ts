import axios, { AxiosRequestConfig } from 'axios';

const axiosInstance = axios.create();

interface RequestConfig<T> extends AxiosRequestConfig {
  data?: T;
}

export const request = <T, R>(config: RequestConfig<T>): Promise<R> => {
  return axiosInstance
    .request(config)
    .then((res) => res.data)
    .catch((error) => {
      // tslint:disable:no-console
      console.log(error);
      return error;
    });
};
